package youtube;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Video{
	private String titulo;
	private int numLikes;
	private Date fechaCreacion;
	private ArrayList <Comentario> comentarios;
	
	public Video(String titulo, Date fechaCreacion) {
        this.titulo = titulo;
        this.fechaCreacion = fechaCreacion;
        this.numLikes = 0;
        this.comentarios = new ArrayList<>();
    }
	

	public boolean mostrarMenu(Scanner leer) {
		boolean volverMenuCanal= false;
		do {
            System.out.println("|---" + titulo+ "---|	");
            System.out.println("1- Nuevo comentario");
            System.out.println("2- Like");
            System.out.println("3- Mostrar comentarios");
            System.out.println("4- Mostrar estadisticas");
            System.out.println("0- Salir");
            System.out.println("|-----------------------|");

            System.out.println("Selecciona una opción: ");
            int opcion = leer.nextInt();
            leer.nextLine();

            switch (opcion) {
                case 1:
                    nuevoComentario(leer);
                    break;
                case 2:
                    darLike();
                    break;
                case 3:
                    mostrarComentarios();
                    break;
                case 4:
                    mostrarEstadisticas();
                    break;
                case 0:
                    volverMenuCanal = true; //para salir del bucle
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
            }

        } while (!volverMenuCanal);

        // Retorna el valor de volverMenuCanal al final del metodo
        return volverMenuCanal;
    }	


	private void nuevoComentario(Scanner leer) {
		System.out.println("Introduce comentario y nombre de usuario:");
        String textoComentario = leer.nextLine();

        String nombreUsuario = leer.nextLine();

        Comentario nuevoComentario = new Comentario(textoComentario, nombreUsuario);
        comentarios.add(nuevoComentario);
	}
	
	
	private void darLike() {
		numLikes++;
		System.out.println("❤️\u001B[31mME GUSTA\u001B[0m");
	}
	
	
	private void mostrarComentarios() {
		if (comentarios.isEmpty()) {
            System.out.println("No hay comentarios para mostrar.");
        } else {
            System.out.println("Comentarios del vídeo:");
            for (Comentario comentario : comentarios) {
                System.out.println("Comentario: \"" + comentario.getTexto() + "\" del usuario: "
                        + comentario.getNombreUsuario() + " en fecha: " + comentario.getFechaCreacion());
            }
        }
	}
	
	
	private void mostrarEstadisticas() {
		System.out.println("Video: \"" + titulo + "\" en fecha: " + fechaCreacion
                + " con " + numLikes + " likes y " + comentarios.size() + " comentarios.");
		System.out.println("Video en tendencia: " + (esVideoPopular() ? "Si" : "No"));
		VerificarSiContieneSpam();
    }
	
	public boolean esVideoPopular() {
        return numLikes >= 10 || comentarios.size() >= 2;
    }
	
	public void VerificarSiContieneSpam() {
        for (Comentario comentario : comentarios) {
            boolean contieneEnlacesExternos = comentario.contieneSpam();
            System.out.println("El comentario de " + comentario.getNombreUsuario() +
                    " contiene spam?: " + (contieneEnlacesExternos ? "Sí" : "No"));
        }
    }
	
	public String getTitulo() {
        return titulo;
    }

    public int getNumLikes() {
        return numLikes;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }
    
    
}
	
	
