package youtube;

import java.util.Scanner;
import java.util.Date;
import java.util.ArrayList;

public class Youtube{
	private ArrayList<Canal> canales;
	private Canal canalSeleccionado;
	
	public Youtube(){
		this.canales = new ArrayList<>();
		this.canalSeleccionado = null;
	}
	
	public void MostrarMenu() {
		Scanner leer = new Scanner(System.in);
        int opcion;
        do {
        	System.out.println("|----YOUTUBE-----|");
            System.out.println("1- Nuevo canal");
            System.out.println("2- Seleccionar canal");
            System.out.println("3- Mostrar estadísticas");
            System.out.println("4- Mostrar estadísticas completas");
            System.out.println("0 - Salir");
            System.out.println("|-----------------|");
            
            
            System.out.print("Selecciona una opción: ");
            opcion = leer.nextInt();
            
            leer.nextLine();

            if (opcion == 1) {
                NuevoCanal(leer);
            } else if (opcion == 2) {
                seleccionarCanal(leer);
                
            } else if (opcion == 3) {
                mostrarEstadisticas();
            } else if (opcion == 4) {
                mostrarEstadisticasCompletas();
            } else if (opcion == 0) {
                System.out.println("");
            } else {
                System.out.println("opción no valida");
            }

        } while (opcion != 0);
        leer.close();
    }
	
	
	
	private void NuevoCanal(Scanner leer) {
		System.out.print("Introduce el nombre del nuevo canal: ");
		String nombreCanal = leer.nextLine();
        Canal nuevoCanal = new Canal(nombreCanal, new Date());
        canales.add(nuevoCanal);
      
        canalSeleccionado = nuevoCanal;
        canalSeleccionado.mostrarMenu(leer);  //lleva a mostrarMenu en canal
	}
	
	private void seleccionarCanal(Scanner leer) {
		if (canales.size()>0) { //si hay canales
            System.out.println("\nSelecciona canal:");
            for (int i = 0; i < canales.size(); i++) {
                System.out.println(i + " - Nombre canal: \"" + canales.get(i).getNombre() + "\" creado en fecha: "
                        + canales.get(i).getFechaCreacion() + " con " + canales.get(i).getVideos().size() + " videos");
            }

            System.out.print("Selecciona un canal: ");
            int indiceCanal = leer.nextInt();
            
            leer.nextLine(); // consume el salto de linea

            if (indiceCanal >= 0 && indiceCanal < canales.size()) {
                canalSeleccionado = canales.get(indiceCanal);
                canalSeleccionado.mostrarMenu(leer);
            } else {
                System.out.println("Índice de canal no válido.");
            }
        } else {
            System.out.println("No hay canales disponibles. Crea uno.");
            NuevoCanal(leer);
        }
    }

	
	private void mostrarEstadisticas() {
		System.out.println("Youtube tiene " + canales.size() + (canales.size() == 1 ? " canal" : " canales"));
	}
	
	private void mostrarEstadisticasCompletas() {              ///////////////////////  FALTAR ACABAR dejar para ultimo
		if (canales.size() >0) {
			for (Canal canal : canales) {
            System.out.println("- Nombre del canal: \"" + canal.getNombre() + "\" creado en fecha: " + canal.getFechaCreacion()
                    + " con " + canal.getVideos().size() + " video(s)");

            for (Video video : canal.getVideos()) {
                System.out.println("-- Video: \"" + video.getTitulo() + "\" en fecha: " + video.getFechaCreacion()+
                         " con " + video.getNumLikes() + " likes y " + video.getComentarios().size() + " comentario(s)");

                for (Comentario comentario : video.getComentarios()) {
                    System.out.println("--- Comentario: \"" + comentario.getTexto() + "\" del usuario: "
                            + comentario.getNombreUsuario() + " en fecha: " + comentario.getFechaCreacion());
                }
            }
        }
        System.out.println("");
			
		} else {
			System.out.println("No hay canales existentes.\n");
		}
    }
	
	
	// MAIN PROGRAM  ------------------------------------------------------------------
	public static void main(String[] args) {
		Youtube youtube = new Youtube();
		youtube.MostrarMenu();
}

}