package youtube;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Date;

public class Canal{
	private String nombre;
	private ArrayList<Video> videos;
	private Date fechaCreacion;
	
	public Canal(String nombre, Date fechaCreacion) {
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.videos = new ArrayList<>();
    }
	
	public void mostrarMenu(Scanner leer) {
		boolean VolverMenuYoutube = false;

        do {
            System.out.println("\n|--- " + nombre + " ---|");
            System.out.println("1- Nuevo video");
            System.out.println("2- Seleccionar video");
            System.out.println("3- Mostrar estadísticas");
            System.out.println("4- Mostrar info videos");
            System.out.println("0- Salir");
            System.out.println("|-----------------------|");

            System.out.println("Selecciona una opción: ");
            int opcion = leer.nextInt();
            leer.nextLine();

            switch (opcion) {
                case 1:
                    nuevoVideo(leer);
                    break;
                case 2:
                    seleccionarVideo(leer);
                    break;
                case 3:
                    mostrarEstadisticas();
                    break;
                case 4:
                    mostrarInfoVideos();
                    break;
                case 0:
                    VolverMenuYoutube = true;
                    break;
                default:
                    System.out.println("Opción no válida. Inténtalo de nuevo.");
            }

        } while (!VolverMenuYoutube);
    }
	
	private void nuevoVideo(Scanner leer) {
        System.out.print("Ingrese el nombre del nuevo video: ");
        String nombreVideo = leer.nextLine();
        Video nuevoVideo = new Video(nombreVideo, new Date());
        videos.add(nuevoVideo);
    }
	
	private void seleccionarVideo(Scanner leer) {
		if (videos.size()>0) {
            System.out.println("Selecciona video:");
            for (int i = 0; i < videos.size(); i++) {
                System.out.println(i + "-Video: \"" + videos.get(i).getTitulo() + "\" en fecha: "
                        + videos.get(i).getFechaCreacion() + " con " + videos.get(i).getNumLikes() + " likes y "
                        + videos.get(i).getComentarios().size() + " comentarios.");
            }

            System.out.print("Selecciona un video: ");
            int indiceVideo = leer.nextInt();
            leer.nextLine();

            if (indiceVideo >= 0 && indiceVideo < videos.size()) {
                Video videoSeleccionado = videos.get(indiceVideo);
                videoSeleccionado.mostrarMenu(leer);
            } else {
                System.out.println("Índice de video no válido.");
            }
        } else {
            System.out.println("No hay videos disponibles. Crea uno automáticamente.");
            nuevoVideo(leer);
        }
    }
	
	
	public void mostrarEstadisticas() {
		System.out.println("Nombre del canal: \"" + nombre + "\" creado en fecha " + fechaCreacion
                + " con " + videos.size() + " videos.");
		 System.out.println("Total de likes en el canal: " + getTotalLikes()); //para saber la popularidad del canal
	}
    

	private void mostrarInfoVideos() {
		for (Video video : videos) {
            System.out.println(video);
        }
    }
	
	public int getTotalLikes() { 		
        int totalLikes = 0;
        for (Video video : videos) {
            totalLikes += video.getNumLikes();
        }
        return totalLikes;
    }
		
	public String getNombre() {
		return nombre;
	}
	
	public ArrayList<Video> getVideos() {
		return videos;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}
}