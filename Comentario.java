package youtube;

import java.util.Date;

public class Comentario{
	private String nombreUsuario;
	private String texto;
	private Date fechaCreacion;

	public Comentario(String texto, String nombreUsuario) {
		this.texto = texto;
        this.nombreUsuario = nombreUsuario;
        this.fechaCreacion = new Date();
    }
	
	public boolean contieneSpam() {							//controlar si un comentario tiene spam
        return texto.contains("http://") || texto.contains("https://");
    }
	
	public String getNombreUsuario() {
        return nombreUsuario;
    }

	public String getTexto() {
        return texto;
    }

	public Date getFechaCreacion() {
        return fechaCreacion;
    }
}